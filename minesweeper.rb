# encoding: utf-8

class Minesweeper

  MARK_OF_ABSENT_MINE = 0
  MARK_OF_PRESENT_MINE = 1

  PUBLIC_FIELD_HIDDEN_CELL = '?'

  ERROR_MESSAGE_ENTERED_INVALID_INFO = "--- Seems like you've entered invalid information, please, try again \n---> "

  def initialize
    print_out_invitation_message
    until get_validate_and_init_game_params!; end
    init_variables!
    put_mines!
    start_the_game
  end


  private

  def print_out_invitation_message
    print "\n"
    print "--- Hello dude! \n"
    print "--- You just launched Minesweeper game \n"
    print "--- Please, enter the number of cols, rows and mines separated by comma \n"
    print "# Example: 3,3,2 \n"
    print "---> "
  end

  def get_validate_and_init_game_params!
    params         = gets.chomp.delete(' ')
    format_regex   = /\d+,\d+,\d+$/
    min_rows_count = 2
    min_cols_count = 2

    # validate format
    unless params =~ format_regex
      print ERROR_MESSAGE_ENTERED_INVALID_INFO
      return false
    end
    # validate values
    params = params.split(',').map(&:to_i)
    unless (params.fetch(0) >= min_rows_count) &&
           (params.fetch(1) >= min_cols_count) &&
           (params.fetch(0) * params.fetch(1) > params.fetch(2)) &&
           (params.fetch(2) >= 0)

      print ERROR_MESSAGE_ENTERED_INVALID_INFO
      return false
    end

    @settings = {
        field_cols_count: params.fetch(0), # X
        field_rows_count: params.fetch(1), # Y
        mines_count:      params.fetch(2)
    }
  end

  def init_variables!
    @field_size   = @settings[:field_cols_count] * @settings[:field_rows_count]
    @real_field   = Array.new(@field_size, MARK_OF_ABSENT_MINE)
    @public_field = Array.new(@field_size, PUBLIC_FIELD_HIDDEN_CELL)
    @game_over    = false
  end

  def put_mines!
    array_of_field_indexes = [*0...@field_size]

    @settings[:mines_count].times do
      random_index              = array_of_field_indexes.sample
      @real_field[random_index] = MARK_OF_PRESENT_MINE
      array_of_field_indexes   -= [random_index]
    end
  end

  def start_the_game
    print "--- Alright, let's get started \n"

    until @game_over
      print "\n"
      print_out_current_field_state_for_field(@public_field)
      print "\n"
      print "--- Enter coordinates of the cell you'd like to check \n"
      print "# Example: 0,0 \n"
      print "---> "
      until get_validate_and_process_coordinates_from_player!; end
    end

    exit
  end

  def print_out_current_field_state_for_field(field)
    number_of_symbols_to_overflow = 15
    x_scale_string                = [*0...@settings[:field_cols_count]].join(',').delete(',')
    y_range_to_iterate_through    = 0...@settings[:field_rows_count]
    length_of_the_empty_row       = @settings[:field_cols_count] + number_of_symbols_to_overflow


    print "|#{'=' * length_of_the_empty_row}\n"
    print "|#{' ' * length_of_the_empty_row}\n"
    print "|   X#{x_scale_string}\n"
    print "|  Y \n"
    y_range_to_iterate_through.each do |index|
      row_of_elements_as_string = row_elements_by_row_index(field, index).join(',').delete(',')
      print "|  #{index} #{row_of_elements_as_string}\n"
    end
    print "|#{' ' * length_of_the_empty_row}\n"
    print "|#{' ' * length_of_the_empty_row}\n"
    print "|#{' ' * length_of_the_empty_row}\n"
  end

  def row_elements_by_row_index(field, index)
    cols_count     = @settings[:field_cols_count]
    elements_range = (cols_count * index)...(cols_count * (index + 1))
    field[elements_range]
  end

  def get_validate_and_process_coordinates_from_player!
    raw_coordinates = gets.chomp.delete(' ')
    format_regex    = /\d+,\d+$/

    # validate format
    unless raw_coordinates =~ format_regex
      print ERROR_MESSAGE_ENTERED_INVALID_INFO
      return false
    end

    # validate values
    raw_coordinates = raw_coordinates.split(',').map(&:to_i)
    unless (raw_coordinates.min >= 0) &&
           (raw_coordinates.fetch(0) < @settings[:field_cols_count]) &&
           (raw_coordinates.fetch(1) < @settings[:field_rows_count])

      print ERROR_MESSAGE_ENTERED_INVALID_INFO
      return false
    end

    chosen_index = coordinates_to_field_index(raw_coordinates)
    process_the_chosen_index(chosen_index)
  end

  def coordinates_to_field_index(array_of_x_y)
    array_of_x_y.fetch(1) * @settings[:field_cols_count] + array_of_x_y.fetch(0)
  end

  def process_the_chosen_index(index)
    if @real_field.fetch(index) == MARK_OF_PRESENT_MINE
      game_over!
    else
      number_of_nearer_mines = number_of_nearer_mines_on_index(index)

      if number_of_nearer_mines == MARK_OF_ABSENT_MINE
        @public_field[index] = MARK_OF_ABSENT_MINE

        set_mines_count_around_the_index(index)
      else
        @public_field[index] = number_of_nearer_mines
      end

      you_won! if are_all_the_empty_cells_opened?
    end

    return 'coordinates were processed' # to break the get_validate_and_process_coordinates_from_player! loop
  end

  def set_mines_count_around_the_index(index)
    available_indexes = available_indexes_to_search_around_index(index)
    available_cells_values = available_indexes.map { |index| number_of_nearer_mines_on_index(index) }

    zeros_everywhere = available_cells_values.count(MARK_OF_ABSENT_MINE) == available_cells_values.length

    if zeros_everywhere
      available_indexes.each do |available_index|
        @public_field[available_index] = MARK_OF_ABSENT_MINE
      end
    else
      indexes_and_values_of_available_cells = Hash[available_indexes.zip(available_cells_values)]
      indexes_and_values_of_available_cells.each do |index, value|
        @public_field[index] = value
      end
    end
  end

  def number_of_nearer_mines_on_index(index)
    available_indexes = available_indexes_to_search_around_index(index)
    available_cells = available_indexes.map { |index_to_check| @real_field[index_to_check] }
    available_cells.count(MARK_OF_PRESENT_MINE)
  end

  def available_indexes_to_search_around_index(index)
    raw_upper_index = index - @settings[:field_cols_count]
    raw_lower_index = index + @settings[:field_cols_count]
    center_top = raw_upper_index < 0 ? nil : raw_upper_index # cell_index_upper_the_current
    center_bottom = raw_lower_index >= @field_size ? nil : raw_lower_index # cell_index_lower_the_current

    prev_top = prev_cell_for_index(center_top)
    next_top = next_cell_for_index(center_top)

    prev_center = prev_cell_for_index(index)
    next_center = next_cell_for_index(index)

    prev_bottom = prev_cell_for_index(center_bottom)
    next_bottom = next_cell_for_index(center_bottom)

    [
      prev_top,
      center_top,
      next_top,
      prev_center,
      next_center,
      prev_bottom,
      center_bottom,
      next_bottom
    ].compact
  end

  def prev_cell_for_index(given_index)
    return nil if given_index.nil? || given_index <= 0
    previous_index = given_index - 1
    return nil unless index_on_the_same_row_to_index?(given_index, previous_index)
    previous_index
  end

  def next_cell_for_index(given_index)
    return nil if given_index.nil?
    next_index = given_index + 1
    return nil unless index_on_the_same_row_to_index?(given_index, next_index)
    next_index < @field_size ? next_index : nil
  end

  def index_on_the_same_row_to_index?(first_index, second_index)
    first_row = first_index / @settings[:field_cols_count]
    second_row = second_index / @settings[:field_cols_count]
    first_row == second_row
  end

  def are_all_the_empty_cells_opened?
    @public_field.count(PUBLIC_FIELD_HIDDEN_CELL) == @settings[:mines_count]
  end

  def you_won!
    set_game_over!
    print "\n"
    print "--- Hooray! You won! \n"
    print "# Here's the field state \n"
    print_out_current_field_state_for_field(@real_field)
  end

  def game_over!
    set_game_over!
    print "\n"
    print "--- Boom!! \n"
    print "--- You hit the mine! \n"
    print "--- GAME OVER \n"
    print "# Here's the field state \n"
    print_out_current_field_state_for_field(@real_field)
  end

  def set_game_over!
    @game_over = true
  end
end

Minesweeper.new
